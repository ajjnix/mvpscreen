import UIKit

final class CountryTableViewCell: UITableViewCell {
    private let title = UILabel()
    private let options = UIStackView()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureContentView()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func configureContentView() {
        configureTitle()
        configureOptionsView()

        let stackView = UIStackView(arrangedSubviews: [title, options])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(stackView)
        NSLayoutConstraint.activate([
            stackView.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor),
            stackView.leftAnchor.constraint(equalTo: safeAreaLayoutGuide.leftAnchor, constant: 60),
            stackView.rightAnchor.constraint(lessThanOrEqualTo: safeAreaLayoutGuide.rightAnchor, constant: 8),
        ])
        stackView.axis = .vertical
        stackView.spacing = 2
    }

    private func configureTitle() {
        title.font = UIFont.systemFont(ofSize: 14)
    }

    private func configureOptionsView() {
        options.spacing = 4
    }

    func update(_ country: Country) {
        title.text = country.name
        options.arrangedSubviews.forEach({ $0.removeFromSuperview() })
        country.options
            .map({ $0.rawValue })
            .sorted(by: { $0 < $1 })
            .map(makeOptionLabel)
            .forEach(options.addArrangedSubview)
        options.addArrangedSubview(makeSpacerView())
    }

    private func makeOptionLabel(text: String) -> UILabel {
        let label = UILabel()
        label.text = text
        label.font = UIFont.boldSystemFont(ofSize: 12)
        return label
    }

    private func makeSpacerView() -> UIView {
        let spacerView = UIView()
        spacerView.setContentHuggingPriority(.defaultLow, for: .horizontal)
        return spacerView
    }
}
