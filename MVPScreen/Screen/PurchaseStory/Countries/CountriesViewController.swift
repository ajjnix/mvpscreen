import UIKit

final class CountriesViewController: UITableViewController {
    typealias Cell = CountryTableViewCell
    typealias CountryHandler = (Country) -> Void
    let repository: CountriesRepository
    let handler: CountryHandler
    var countries: [Country]

    init(repository: CountriesRepository, handler: @escaping CountryHandler) {
        self.repository = repository
        self.countries = repository.countries
        self.handler = handler
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        configureSearchController()
        title = "Select a Country"
    }

    private func configureTableView() {
        clearsSelectionOnViewWillAppear = true
        tableView.reloadData()
        tableView.rowHeight = 48
        tableView.register(Cell.self, forCellReuseIdentifier: String(describing: Cell.self))
    }

    private func configureSearchController() {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Countries..."
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countries.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: String(describing: Cell.self), for: indexPath)
    }

    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? Cell else {
            assertionFailure()
            return
        }
        cell.update(countries[indexPath.row])
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        handler(countries[indexPath.row])
    }
}

extension CountriesViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if let text = searchController.searchBar.text, text.isEmpty.not {
            countries = repository.find(by: text)
        } else {
            countries = repository.countries
        }
        tableView.reloadData()
    }
}
