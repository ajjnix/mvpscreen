import UIKit

final class PurchaseUserStoryRouter {
    private let navigationViewController = UINavigationController()

    static func makeUserStory() -> (PurchaseUserStoryRouter, UIViewController) {
        let router = PurchaseUserStoryRouter()
        return (router, router.makeInitialScreen())
    }

    private func makeInitialScreen() -> UIViewController {
        let viewController = CountriesViewController(
            repository: StubCountriesRepository(),
            handler: { [weak self] country in
                self?.appearChoiceSubscriptionViewController()
        })
        navigationViewController.viewControllers = [viewController]
        return navigationViewController
    }

    private func appearChoiceSubscriptionViewController() {
        let viewController = makeChoiceSubscriptionViewController()
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.navigationBar.setTranslucent()
        navigationViewController.present(navigationController, animated: true, completion: nil)
    }

    private func makeChoiceSubscriptionViewController() -> ChoiceSubscriptionViewController {
        return ChoiceSubscriptionViewController(
            repository: StubSubscriptionPackageRepository(),
            close: { [weak self] in
                self?.navigationViewController.dismiss(animated: true, completion: nil)
        })
    }
}
