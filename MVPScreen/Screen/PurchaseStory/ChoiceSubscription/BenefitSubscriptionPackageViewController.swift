import UIKit

final class BenefitSubscriptionPackageViewController: UIViewController {
    private let benefit = UILabel()

    init(_ benefit: String, textColor: UIColor) {
        self.benefit.text = benefit
        self.benefit.textColor = textColor
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configurePromo()
        benefit.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(benefit)
        NSLayoutConstraint.activate([
            benefit.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -16),
            benefit.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -32),
            benefit.centerXAnchor.constraint(equalTo: view.centerXAnchor),
        ])
    }

    private func configurePromo() {
        benefit.textAlignment = .center
    }
}
