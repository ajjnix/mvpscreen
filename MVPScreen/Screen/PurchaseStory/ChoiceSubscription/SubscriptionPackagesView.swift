import UIKit

protocol SubscriptionPackagesViewDelegate: class {
    func subscriptionPackagesView(_ view: SubscriptionPackagesView, selected subscriptionPackage: SubscriptionPackage)
}

final class SubscriptionPackagesView: UIView {
    private let highlighter: GradientHighlighterView
    private let stackView = UIStackView()
    private let subscriptionPackages: [SubscriptionPackage]
    private var highlightedIndex = 0
    weak var delegate: SubscriptionPackagesViewDelegate?

    init(subscriptionPackages: [SubscriptionPackage], highlight: [UIColor]) {
        self.subscriptionPackages = subscriptionPackages
        highlighter = GradientHighlighterView(width: 4, colors: highlight, direction: .topToBottom)
        super.init(frame: .zero)
        configureStackView()
        stackView.frame = bounds
        stackView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(stackView)
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func select(_ subscriptionPackage: SubscriptionPackage) {
        guard let index = subscriptionPackages.lastIndex(of: subscriptionPackage) else {
            assertionFailure()
            return
        }
        selectSubscriptionPackage(at: index)
    }
}

extension SubscriptionPackagesView {
    private func configureStackView() {
        stackView.distribution = .fillEqually
        makeSubscriptionPackageViews().forEach(stackView.addArrangedSubview)
    }

    private func makeSubscriptionPackageViews() -> [UIView] {
        return subscriptionPackages.map({ package in
            let label = UILabel()
            label.text = package.title
            label.textAlignment = .center
            let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onTap(_:)))
            label.addGestureRecognizer(gestureRecognizer)
            label.isUserInteractionEnabled = true
            return label
        })
    }

    @objc
    private func onTap(_ sender: UITapGestureRecognizer) {
        guard let tappedView = sender.view else {
            assertionFailure()
            return
        }
        guard let index = stackView.arrangedSubviews.lastIndex(where: { $0 === tappedView }) else {
            assertionFailure("Can't find tappedView in subscriptions")
            return
        }
        selectSubscriptionPackage(at: index)
    }

    private func selectSubscriptionPackage(at index: Int) {
        guard subscriptionPackages.indices.contains(index) else {
            assertionFailure()
            return
        }
        hightlightSubscriptionPackage(at: index)
        delegate?.subscriptionPackagesView(self, selected: subscriptionPackages[index])
    }

    private func hightlightSubscriptionPackage(at index: Int) {
        highlightedIndex = index
        addSubview(highlighter)
        layoutHighlighterIfNeeded()
    }

    private func layoutHighlighterIfNeeded() {
        let view = stackView.arrangedSubviews[highlightedIndex]
        if highlighter.frame != view.frame {
            highlighter.frame = view.frame
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        stackView.layoutIfNeeded()
        layoutHighlighterIfNeeded()
    }
}


final private class GradientHighlighterView: UIView {
    private let gradientMask = CAShapeLayer()

    init(width: CGFloat, colors: [UIColor], direction: GradientDirection) {
        super.init(frame: .zero)
        configureGradient(width: width, colors: colors, direction: direction)
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        updateGradientMask()
    }

    private func updateGradientMask() {
        gradientMask.path = UIBezierPath(rect: bounds).cgPath
    }

    private func configureGradient(width: CGFloat, colors: [UIColor], direction: GradientDirection) {
        let gradientLayer = layer as! CAGradientLayer
        gradientLayer.setDirection(direction)
        gradientLayer.colors = colors.map({ $0.cgColor })
        gradientLayer.mask = gradientMask

        gradientMask.lineWidth = width
        gradientMask.fillColor = nil
        gradientMask.strokeColor = UIColor.black.cgColor
    }
}
