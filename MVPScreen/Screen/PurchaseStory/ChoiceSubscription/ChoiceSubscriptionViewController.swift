import UIKit

final class ChoiceSubscriptionViewController: UIViewController {
    private let close: () -> Void
    private let subscriptionPackagesView: SubscriptionPackagesView
    private let repository: SubscriptionPackageRepository
    private let benefitsPageViewController: UIPageViewController
    private let pageControl = UIPageControl()
    private var benefitViewControllers: [UIViewController] = []

    init(repository: SubscriptionPackageRepository, close: @escaping () -> Void) {
        self.close = close
        self.repository = repository
        self.subscriptionPackagesView = SubscriptionPackagesView(
            subscriptionPackages: repository.subscriptionPackage,
            highlight: ColorScheme().choiceSubscription.highlightSubscriptionPackageGradient
        )
        self.benefitsPageViewController = UIPageViewController(
            transitionStyle: .scroll,
            navigationOrientation: .horizontal,
            options: nil
        )
        super.init(nibName: nil, bundle: nil)
        self.subscriptionPackagesView.delegate = self
        self.benefitsPageViewController.dataSource = self
        self.benefitsPageViewController.delegate = self
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = ColorScheme().choiceSubscription.background
        configureNavigationItem()
        configureLayout()
        subscriptionPackagesView.select(repository.promoSubscription)
    }

    private func configureLayout() {
        addChild(benefitsPageViewController)
        benefitsPageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(benefitsPageViewController.view)
        benefitsPageViewController.didMove(toParent: self)
        NSLayoutConstraint.activate([
            benefitsPageViewController.view.topAnchor.constraint(equalTo: view.topAnchor),
            benefitsPageViewController.view.leftAnchor.constraint(equalTo: view.leftAnchor),
            benefitsPageViewController.view.rightAnchor.constraint(equalTo: view.rightAnchor),
            benefitsPageViewController.view.heightAnchor.constraint(equalToConstant: 288),
        ])

        pageControl.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(pageControl)
        NSLayoutConstraint.activate([
            pageControl.topAnchor.constraint(equalTo: benefitsPageViewController.view.bottomAnchor),
            pageControl.centerXAnchor.constraint(equalTo: view.centerXAnchor),
        ])

        subscriptionPackagesView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(subscriptionPackagesView)
        NSLayoutConstraint.activate([
            subscriptionPackagesView.leftAnchor.constraint(equalTo: view.leftAnchor),
            subscriptionPackagesView.rightAnchor.constraint(equalTo: view.rightAnchor),
            subscriptionPackagesView.heightAnchor.constraint(equalToConstant: 88),
            subscriptionPackagesView.topAnchor.constraint(equalTo: pageControl.bottomAnchor),
        ])

        let activate = makeActivateButton()
        activate.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(activate)
        NSLayoutConstraint.activate([
            activate.topAnchor.constraint(equalTo: subscriptionPackagesView.bottomAnchor, constant: 24),
            activate.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            activate.heightAnchor.constraint(equalToConstant: 46),
        ])

        let background = makeBackground()
        background.translatesAutoresizingMaskIntoConstraints = false
        view.insertSubview(background, belowSubview: benefitsPageViewController.view)
        NSLayoutConstraint.activate([
            background.topAnchor.constraint(equalTo: benefitsPageViewController.view.topAnchor),
            background.bottomAnchor.constraint(equalTo: pageControl.bottomAnchor),
            background.leftAnchor.constraint(equalTo: view.leftAnchor),
            background.rightAnchor.constraint(equalTo: view.rightAnchor),
        ])
    }

    private func makeActivateButton() -> UIButton {
        let button = GradientButton(type: .system)
        button.gradientColors = ColorScheme().choiceSubscription.activeButtonGradient
        button.setTitle("ACTIVATE", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.contentEdgeInsets = UIEdgeInsets.init(top: 6, left: 50, bottom: 6, right: 50)
        button.layer.cornerRadius = 23
        return button
    }

    private func makeBackground() -> UIView {
        let background = GradientView()
        background.gradientColors = ColorScheme().choiceSubscription.backgroundBenefitsGradient
        background.direction = .topToBottom
        return background
    }
}

extension ChoiceSubscriptionViewController {
    private func configureNavigationItem() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .cancel,
            target: self,
            action: #selector(onClose(_:))
        )
        navigationItem.rightBarButtonItem?.tintColor = ColorScheme().choiceSubscription.navigationBar
        let title = UILabel()
        title.text = "+1 858-264-0510"
        title.font = UIFont.boldSystemFont(ofSize: 16)
        title.textColor = ColorScheme().choiceSubscription.navigationBar
        navigationItem.titleView = title
    }

    @objc
    private func onClose(_ sender: UIBarButtonItem) {
        close()
    }
}

extension ChoiceSubscriptionViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        return benefitViewControllers.firstIndex(where: { $0 === viewController })
            .map(benefitViewControllers.index(before:))
            .flatMap({ atIndex in
                if benefitViewControllers.indices.contains(atIndex) {
                    return benefitViewControllers[atIndex]
                }
                return benefitViewControllers.last
            })
    }

    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        return benefitViewControllers.firstIndex(where: { $0 === viewController })
            .map(benefitViewControllers.index(after:))
            .flatMap({ atIndex in
                if benefitViewControllers.indices.contains(atIndex) {
                    return benefitViewControllers[atIndex]
                }
                return benefitViewControllers.first
            })
    }

    func pageViewController(_ pageViewController: UIPageViewController,
                            didFinishAnimating finished: Bool,
                            previousViewControllers: [UIViewController],
                            transitionCompleted completed: Bool) {
        if let index = pageViewController.viewControllers?.first.flatMap(benefitViewControllers.firstIndex), completed {
            pageControl.currentPage = index
        }
    }
}

extension ChoiceSubscriptionViewController: SubscriptionPackagesViewDelegate {
    func subscriptionPackagesView(_ view: SubscriptionPackagesView, selected subscription: SubscriptionPackage) {
        updateBenefits(for: subscription)
    }

    private func updateBenefits(for subscription: SubscriptionPackage) {
        benefitViewControllers = subscription.benefits.map({ benefit in
            return BenefitSubscriptionPackageViewController(benefit, textColor: ColorScheme().choiceSubscription.benefits)
        })
        benefitsPageViewController.setViewControllers([benefitViewControllers.first!], direction: .forward, animated: false, completion: nil)
        pageControl.numberOfPages = subscription.benefits.count
        pageControl.currentPage = 0
    }
}
