import UIKit

final class ColorScheme {
    final class ChoiceSubscriptionScreen {
        let activeButtonGradient: [UIColor] = [
            UIColor.init(red: 67/255, green: 118/255, blue: 214/255, alpha: 1),
            UIColor.init(red: 113/255, green: 185/255, blue: 250/255, alpha: 1),
        ]

        let highlightSubscriptionPackageGradient: [UIColor] = [
            UIColor(red: 111/255, green: 183/255, blue: 239/255, alpha: 1),
            UIColor(red: 68/255, green: 120/255, blue: 210/255, alpha: 1),
        ]

        let backgroundBenefitsGradient: [UIColor] = [
            UIColor(red: 41/255, green: 68/255, blue: 155/255, alpha: 1),
            UIColor(red: 96/255, green: 111/255, blue: 233/255, alpha: 1),
        ]

        let background = UIColor.white
        let navigationBar = UIColor.white
        let benefits = UIColor.white
    }

    let choiceSubscription = ChoiceSubscriptionScreen()
}
