import UIKit

enum GradientDirection {
    case topToBottom
    case leftToRight

    var start: CGPoint {
        switch self {
        case .topToBottom:
            return CGPoint(x: 0.5, y: 0)
        case .leftToRight:
            return CGPoint(x: 0, y: 0.5)
        }
    }

    var end: CGPoint {
        switch self {
        case .topToBottom:
            return CGPoint(x: 0.5, y: 1)
        case .leftToRight:
            return CGPoint(x: 1, y: 0.5)
        }
    }
}

extension CAGradientLayer {
    func setDirection(_ direction: GradientDirection) {
        startPoint = direction.start
        endPoint = direction.end
    }
}
