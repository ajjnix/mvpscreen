import UIKit

final class GradientView: UIView {
    private var gradientLayer: CAGradientLayer {
        return layer as! CAGradientLayer
    }

    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }

    var gradientColors: [UIColor] = [] {
        didSet {
            configureGradient()
        }
    }
    var direction: GradientDirection = .leftToRight {
        didSet {
            configureGradient()
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        updateGradientLayout()
    }

    func configureGradient() {
        gradientLayer.colors = gradientColors.map({ $0.cgColor })
        gradientLayer.setDirection(direction)
    }

    func updateGradientLayout() {
        gradientLayer.cornerRadius = layer.cornerRadius
        gradientLayer.frame = bounds
    }
}
