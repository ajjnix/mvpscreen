import UIKit

final class GradientButton: UIButton {
    private let gradientLayer = CAGradientLayer()
    var gradientColors: [UIColor] = [] {
        didSet {
            configureGradient()
        }
    }
    var direction: GradientDirection = .leftToRight {
        didSet {
            configureGradient()
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        updateGradientLayout()
    }

    private func configureGradient() {
        gradientLayer.setDirection(direction)
        gradientLayer.colors = gradientColors.map({ $0.cgColor })
        layer.insertSublayer(gradientLayer, at: 0)
    }

    private func updateGradientLayout() {
        gradientLayer.cornerRadius = layer.cornerRadius
        gradientLayer.frame = bounds
    }
}
