import Foundation

extension Bool {
    var not: Bool {
        return !self
    }
}
