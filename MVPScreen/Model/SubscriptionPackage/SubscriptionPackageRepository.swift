import Foundation

protocol SubscriptionPackageRepository {
    var subscriptionPackage: [SubscriptionPackage] { get }
    var promoSubscription: SubscriptionPackage { get }
}

final class StubSubscriptionPackageRepository: SubscriptionPackageRepository {
    lazy var subscriptionPackage: [SubscriptionPackage] = {
        let sp1 = SubscriptionPackage(title: "3 months", benefits: [
            "3 months benefit 1",
            "3 months benefit 2",
            "3 months benefit 3",
        ])
        let sp2 = SubscriptionPackage(title: "6 months", benefits: [
            "6 months benefit 1",
            "6 months benefit 2",
            "6 months benefit 3",
            "6 months benefit 4"
        ])
        let sp3 = SubscriptionPackage(title: "12 months", benefits: [
            "12 months benefit 1",
            "12 months benefit 2",
            "12 months benefit 3",
            "12 months benefit 4",
            "12 months benefit 5"
        ])
        return [sp1, sp2, sp3]
    }()

    var promoSubscription: SubscriptionPackage {
        return subscriptionPackage[1]
    }
}
