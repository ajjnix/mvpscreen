import Foundation

struct SubscriptionPackage: Equatable {
    let title: String
    let benefits: [String]
}
