import Foundation

struct Country {
    enum Option: String {
        case call
        case sms
    }
    let options: Set<Option>
    let name: String
}
