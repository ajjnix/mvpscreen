import Foundation

protocol CountriesRepository {
    var countries: [Country] { get }
    func find(by text: String) -> [Country]
}

final class StubCountriesRepository: CountriesRepository {
    lazy var countries: [Country] = {
        return [
            Country(options: [.call, .sms], name: "United States"),
            Country(options: [.call, .sms], name: "Canada"),
            Country(options: [.call], name: "Australia"),
            ]
    }()

    func find(by text: String) -> [Country] {
        let lowercasedText = text.lowercased()
        return countries.filter({ $0.name.lowercased().contains(lowercasedText) })
    }
}
